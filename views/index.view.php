<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="/css/app.css?t=<?= time() ?>">
</head>


<body>

    <div class="container">
        <h1>Example APP</h1>
        <ul class="user-list">
            <li   class="template hide">{{ firstname }} {{ lastname}}</li>
        </ul>
    </div>

</body>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script type="module" src="/js/main.js?t=<?= time() ?>"></script>

</html>