<?php

namespace App;


class User
{

    public $firstname;
    public $lastname;


    function __constructor()
    {
    }


    public static function create($firstname, $lastname)
    {
        $user = new User();
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        return $user;
    }
}
