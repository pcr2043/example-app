<?php

namespace App\Tools;

use App\Logic\Users;

class Render
{


  public function __construct()
  {
  }

  public static function router()
  {

    if (isset($_GET['action'])) {
     
      $data =  json_encode((new Users)->all());
      echo $data;
      die();
    }
  }

  public static function view($template, $data)
  {
    include($template);

    // $view = file_get_contents($template);
    // echo $view; 
    //$view  = self::parse($view, $data);

  }

  public static function parse($view, $data = [])
  {

    $render = new Render();

    return $render->parseForeach($view, $data);
  }

  function parseForeach($view, $data = [])
  {

    $positions = $this->strpos_all($view, ' @foreach');

    foreach ($positions as $index => $position) {


      $foreach = substr($view, $position);

      $indexEndForeach = strpos($foreach, '@endforeach');

      $foreachStarSIndex = strpos($foreach, '(') + 1;

      $foreachEndSIndex = strpos($foreach, ')');



      $statement =  substr($foreach, $foreachStarSIndex, $foreachEndSIndex - $foreachStarSIndex);

      $template =  substr($foreach, $foreachEndSIndex + 1, $indexEndForeach - $foreachEndSIndex  - 1);

      $sIterator = strpos($statement, '$');
      $eIterator = strpos($statement, ' ');
      $iterator = substr($statement, $sIterator, $eIterator - $sIterator);

      $iteratorData = str_replace('$', '', trim(str_replace('in ', '', substr($statement, $eIterator)), ' '));



      $strForeach = '';

      foreach ($data[$iteratorData] as $row) {
        $strForeach .= $template . $row;
      }

      if ($index == 0) {

        $view = str_replace($foreach, $strForeach, $view);

        break;
      }
    }

    return $view;
  }


  function strpos_all($haystack, $needle_regex)
  {
    preg_match_all('/' . $needle_regex . '/', $haystack, $matches, PREG_OFFSET_CAPTURE);
    return array_map(function ($v) {
      return $v[1];
    }, $matches[0]);
  }
}
