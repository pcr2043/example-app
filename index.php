<?php

use App\Logic\Objects;
use App\Logic\Users;
use App\Tools\Render;

require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/vendor/autoload.php';

$users = new Users();
$objects = new Objects();

$data = [
    'data' => [12, 2, 3]
];



Render::router();
Render::view('./views/index.view.php', $data);

