

import UITools from "./UI.js";
import User from "./User.js";


window.UI = new UITools();
class App {

    constructor() {
        this.loaded = false;
    }

    init() {
       
       
        console.log(new User().name)
       
        axios.get('/?action=init').then(response => {

            let data = response.data;
            this.loaded = true;
            UI.List('.user-list', data);
        })
    }
}

window.onload = () => {

    let app = new App();
   app.init();
    console.log(app.loaded)

}