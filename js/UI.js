export default class UI {


    List(ref, data) {

        let el = document.querySelector(ref);

        let template = document.querySelector(ref + ' .template');

        data.forEach(row => {
            
            let cloneTemplate = template.cloneNode(true);
            let keys = Object.keys(row);


            let html = cloneTemplate.innerHTML;

            keys.forEach(key =>{
                  
               html = html.replace(key, row[key]).replaceAll('{{', '').replaceAll('}}', '')
              
            })

       
            cloneTemplate.innerHTML = html;
            cloneTemplate.classList.remove('hide')

            el.append(cloneTemplate)
        });



    }
}